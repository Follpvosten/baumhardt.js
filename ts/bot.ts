import * as TelegramBot from 'node-telegram-bot-api';
import * as fs from 'fs';
import * as moment from 'moment';
import * as lenny from 'lenny';

import { Countdown } from './interfaces';

console.log('Loading bot token...');
if (!fs.existsSync("token.txt")) {
    console.log("FATAL: Please provide a bot token in token.txt!");
    fs.writeFileSync("token.txt", "", { encoding: "utf-8" });
    process.exit(1);
}
const token = fs.readFileSync("token.txt", "utf-8").trim();
if (!token) {
    console.log("FATAL: Please provide a bot token in token.txt!");
    process.exit(2);
}

// Load countdowns
const CDS_FILE = "countdowns.json";
let countdowns: Countdown[] = [];
if (fs.existsSync(CDS_FILE)) {
    console.log('Loading countdowns...');
    countdowns = JSON.parse(fs.readFileSync("countdowns.json", "utf-8"));
}
const writeData = () => fs.writeFile(
    CDS_FILE, JSON.stringify(countdowns),
    err => { if (err) console.error(err); });
const findCountdown = (name: string) =>
    countdowns.find(c => c.name.toLowerCase() == name.toLowerCase());

const bot = new TelegramBot(token, { polling: true });

bot.onText(/^\/(start|help)$/i, msg => {
    let sendOptions = { reply_to_message_id: msg.message_id };
    const answer = `Baumhardt v0.0.1

Available commands:
(Note: name|"name" means the countdown's name, with or without quotes(""). \
Quotes are required for names containing spaces.)
/start or /help - Displays this message
/listc - Lists the available countdowns
/putc <name|"name"> <time>  - Creates a new countdown
/getc [name|"name"] - Shows a countdown or the ones you're following
/delc <name|"name"> - Deletes a countdown
/renamec <name|"name"> <new name> - Renames a countdown
/follow <name|"name"> - Follows a countdown
/unfollow <name|"name"> - Unfollows a countdown`;
    bot.sendMessage(msg.chat.id, answer, sendOptions);
});

const sendNotFound = (name: string, msg: TelegramBot.Message) => {
    bot.sendMessage(
        msg.chat.id,
        `Could not find countdown "${name}"!`,
        { reply_to_message_id: msg.message_id }
    );
}

const dateFormats = ["DD-MM-YYYY", "DD-MM-YY", "DD.MM.YYYY HH:mm", "DD-MM-YY HH:mm"];
const displayFormat = dateFormats[2];
const handlePut = (name: string, time: string, msg: TelegramBot.Message) => {
    const sendOptions: TelegramBot.SendMessageOptions = { reply_to_message_id: msg.message_id };
    let m: moment.Moment;
    if (time.startsWith("+")) {
        let vals = time.replace(/\+/g, "").split(" ");
        m = moment().add(Number(vals[0]), vals[1] as moment.unitOfTime.DurationConstructor);
    }
    else
        m = moment(time, dateFormats);
    if (!m.isValid()) {
        bot.sendMessage(msg.chat.id, `Please provide a valid date!`, sendOptions);
        return;
    }
    let existingCd = findCountdown(name);
    if (existingCd) {
        // The countdown already exists.
        // Check the user...
        if (existingCd.creatorUserId != msg.from.id) {
            bot.sendMessage(msg.chat.id, `You cannot edit a countdown you don't own.`, sendOptions);
            return;
        }
        // Set the countdown's time
        existingCd.moment = m.format();
        bot.sendMessage(msg.chat.id, `Countdown updated to ${m.format(displayFormat)}!`, sendOptions);
        writeData();
        return;
    }
    // We have a new countdown!
    let newCd: Countdown = {
        name: name,
        moment: m.format(),
        creatorUserId: msg.from.id,
        intervals: [],
        followerIds: [msg.from.id]
    };
    countdowns.push(newCd);
    bot.sendMessage(msg.chat.id, `You have added the countdown "${name}" until ${m.format(displayFormat)}!`);
    writeData();
};

const formatDuration = (until: moment.Moment) => {
    let seconds = until.diff(moment()) / 1000;
    let s = Math.floor((seconds) % 60),
        m = Math.floor((seconds / 60) % 60),
        h = Math.floor((seconds / (60 * 60)) % 24),
        d = Math.floor(seconds / (60 * 60 * 24));
    return `${d} days, ${h} hours, ${m} minutes, ${s} seconds`;
};
const formatCountdown = (cd: Countdown) => {
    return `Time left until "${cd.name}" (${moment(cd.moment).format(displayFormat)}): 
${formatDuration(moment(cd.moment))}`;
};
const handleGet = (name: string, msg: TelegramBot.Message) => {
    const sendOptions: TelegramBot.SendMessageOptions = { reply_to_message_id: msg.message_id };
    let cds: Countdown[];
    if (!name) {
        // Find the countdown the user is following
        cds = countdowns.filter(c => c.followerIds.includes(msg.from.id));
        if (!cds) {
            bot.sendMessage(msg.chat.id, `You are not currently following any countdowns.`, sendOptions);
            return;
        }
    } else {
        cds = countdowns.filter(c => c.name.toLowerCase() == name.toLowerCase());
        if (!cds) {
            sendNotFound(name, msg);
            return;
        }
    }

    bot.sendMessage(
        msg.chat.id,
        cds.map(cd => formatCountdown(cd)).join("\n"),
        sendOptions);
}

const formatCountdownList = (cd: Countdown, userId: number) => {
    return `${cd.name} - ${moment(cd.moment).format(displayFormat)}`
        + (cd.followerIds.includes(userId) ? " [x]" : "");
}
const handleList = (msg: TelegramBot.Message) => {
    bot.sendMessage(
        msg.chat.id,
        countdowns.map(cd => formatCountdownList(cd, msg.from.id)).join("\n"),
        { reply_to_message_id: msg.message_id }
    );
};

const handleDel = (name: string, msg: TelegramBot.Message) => {
    let cd = findCountdown(name);
    if (!cd) {
        sendNotFound(name, msg);
        return;
    }
    const sendOptions: TelegramBot.SendMessageOptions = { reply_to_message_id: msg.message_id };
    if (cd.creatorUserId != msg.from.id) {
        bot.sendMessage(msg.chat.id, `You cannot delete a countdown you don't own.`, sendOptions);
        return;
    }
    countdowns.splice(countdowns.indexOf(cd), 1);
    bot.sendMessage(msg.chat.id, `Countdown "${cd.name}" deleted successfully.`, sendOptions);
    writeData();
};

const handleRename = (name: string, newName: string, msg: TelegramBot.Message) => {
    let cd = findCountdown(name);
    if (!cd) {
        sendNotFound(name, msg);
        return;
    }
    const sendOptions: TelegramBot.SendMessageOptions = { reply_to_message_id: msg.message_id };
    if (cd.creatorUserId != msg.from.id) {
        bot.sendMessage(msg.chat.id, `You cannot rename a countdown you don't own.`, sendOptions);
        return;
    }
    let oldName = cd.name;
    cd.name = newName;
    bot.sendMessage(msg.chat.id, `Countdown "${oldName}" sucessfully renamed to "${newName}".`, sendOptions);
    writeData();
};

const handleFollow = (name: string, msg: TelegramBot.Message, forGroup = false) => {
    if (forGroup && msg.chat.type == 'private') {
        bot.sendMessage(msg.chat.id, `You can only use /gfollow in groups.`);
        return;
    }
    let cd = findCountdown(name);
    if (!cd) {
        sendNotFound(name, msg);
        return;
    }
    const sendOptions: TelegramBot.SendMessageOptions = { reply_to_message_id: msg.message_id };
    const targetId = forGroup ? msg.chat.id : msg.from.id;
    if (cd.followerIds.includes(targetId)) {
        const answer = (forGroup ? msg.chat.title + ' is' : 'You are') + ` already following "${cd.name}".`;
        bot.sendMessage(msg.chat.id, answer, sendOptions);
        return;
    }
    cd.followerIds.push(targetId);
    const answer = (forGroup ? msg.chat.title + ' is' : 'You are') + ` now following "${cd.name}".`;
    bot.sendMessage(msg.chat.id, answer, sendOptions);
    writeData();
};
const handleUnfollow = (name: string, msg: TelegramBot.Message, forGroup = false) => {
    if (forGroup && msg.chat.type == 'private') {
        bot.sendMessage(msg.chat.id, 'You can only use /gunfollow in groups.');
        return;
    }
    let cd = findCountdown(name);
    if (!cd) {
        sendNotFound(name, msg);
        return;
    }
    const sendOptions: TelegramBot.SendMessageOptions = { reply_to_message_id: msg.message_id };
    const targetId = forGroup ? msg.chat.id : msg.from.id;
    if (!cd.followerIds.includes(targetId)) {
        const answer = (forGroup ? msg.chat.title + ' is' : 'You are') + ` not currently following "${cd.name}".`;
        bot.sendMessage(msg.chat.id, answer, sendOptions);
        return;
    }
    cd.followerIds.splice(cd.followerIds.indexOf(targetId), 1);
    const answer = (forGroup ? msg.chat.title + ' is' : 'You are') + ` no longer following "${cd.name}".`;
    bot.sendMessage(msg.chat.id, answer, sendOptions);
    writeData();
};

// React to general /putc
bot.onText(/^\/putc (?!")(.+)/i, (msg, match) => {
    let name = match[1].split(" ")[0],
        time = match[1].substr(match[1].indexOf(" "));
    handlePut(name, time, msg);
});
// React to name in ""
bot.onText(/^\/putc "(.+)" (.+)/i, (msg, match) => {
    let name = match[1].trim(),
        time = match[2];
    handlePut(name, time, msg);
});

console.log('Registering functions...');

// React to /getc
bot.onText(/^\/getc$/i, msg => handleGet(null, msg));
bot.onText(/^\/getc "(.+)"/i, (msg, match) => handleGet(match[1].trim(), msg));
bot.onText(/^\/getc (?!")(.+)/i, (msg, match) => handleGet(match[1], msg));

// React to /listc
bot.onText(/^\/listc$/i, msg => handleList(msg));

// React to /delc
bot.onText(/^\/delc "(.+)"/i, (msg, match) => handleDel(match[1].trim(), msg));
bot.onText(/^\/delc (?!")(.+)/i, (msg, match) => handleDel(match[1], msg));

// React to /rename
bot.onText(/^\/renamec "(.+)" (.+)/i,
    (msg, match) => handleRename(match[1].trim(), match[2], msg));
bot.onText(/^\/renamec (?!")(.+)/i,
    (msg, match) => handleRename(
        match[1].split(" ")[0],
        match[1].substr(match[1].indexOf(" ")),
        msg)
);

// React to /follow and /unfollow
bot.onText(/^\/follow "(.+)"/i, (msg, match) => handleFollow(match[1].trim(), msg));
bot.onText(/^\/follow (?!")(.+)/i, (msg, match) => handleFollow(match[1], msg));
bot.onText(/^\/unfollow "(.+)"/i, (msg, match) => handleUnfollow(match[1].trim(), msg));
bot.onText(/^\/unfollow (?!")(.+)/i, (msg, match) => handleUnfollow(match[1], msg));

// React to /gfollow and /gunfollow
bot.onText(/^\/gfollow "(.+)"/i, (msg, match) => handleFollow(match[1].trim(), msg, true));
bot.onText(/^\/gfollow (?!")(.+)/i, (msg, match) => handleFollow(match[1], msg, true));
bot.onText(/^\/gunfollow "(.+)"/i, (msg, match) => handleUnfollow(match[1].trim(), msg, true));
bot.onText(/^\/gunfollow (?!")(.+)/i, (msg, match) => handleUnfollow(match[1], msg, true));

// React to @baumhardtbot
bot.onText(/^@baumhardtbot$/, msg => {
    bot.sendMessage(msg.chat.id, lenny(), { reply_to_message_id: msg.message_id });
});

console.log('Registering timer...');

const interval = function () {
    let dataChanged = false;
    countdowns.forEach(cd => {
        // Check if the countdown's time has passed;
        // Add a second in case we're right at XX:00:00
        if (moment().add(1, 's').isAfter(cd.moment)) {
            cd.followerIds.forEach(async fId => {
                bot.sendMessage(fId, `Countdown "${cd.name}" just ended!`);
            });
            console.log(`Countdown "${cd.name}" ended.`);
            countdowns.splice(countdowns.indexOf(cd), 1);
            dataChanged = true;
        }
    });
    if (dataChanged) writeData();
};

setTimeout(
    () => { setInterval(interval, 30000); interval(); },
    moment().add(1, 'm').seconds(1).diff(moment())
);
