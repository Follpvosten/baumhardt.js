export interface Countdown {
    name: string;
    moment: string;
    creatorUserId: number;
    intervals: { chatId: number, interval: number }[];
    followerIds: number[];
}

interface UserSettings {
    autoFollow: boolean;
    notifyOwn: boolean;
}
