# compile typescript
FROM node:16-alpine AS builder
RUN mkdir -p /app/node_modules && chown -R node:node /app
WORKDIR /app

USER node
COPY --chown=node:node package*.json ./
RUN npm install

COPY --chown=node:node . .
RUN npm run tsc

# build the final image
FROM node:16-alpine AS runner
RUN mkdir -p /app/node_modules && chown -R node:node /app
WORKDIR /app

USER node
COPY --chown=node:node package*.json ./
RUN npm install --production
COPY --from=builder /app/js ./js
CMD [ "node", "js/bot.js" ]
